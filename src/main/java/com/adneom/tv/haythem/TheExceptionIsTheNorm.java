package com.adneom.tv.haythem;

public class TheExceptionIsTheNorm {

    public static void main(String[] args) {
        System.out.println(hello("miguel"));
    }

    static String hello(String name) {
        try {
            return dangerousOperation(name);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return null;
        } finally {
            System.out.println("we are done, let's rest");
        }
    }

    private static String dangerousOperation(String name) {
        if (name.length() > 8) {
            throw new IllegalArgumentException("name too long");
        }
        return "hello " + name;
    }

}
