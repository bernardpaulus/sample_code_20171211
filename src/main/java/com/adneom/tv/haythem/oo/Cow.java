package com.adneom.tv.haythem.oo;

public class Cow extends AbstractAnimal {

    @Override
    public  String eat(String food) {
        if("meat".equals(food)){
            return food + " not yum yum";
        }
        return food + "! Yum yum :)";
    }

}
