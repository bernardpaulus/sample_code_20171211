package com.adneom.tv.haythem.oo;

import org.junit.Assert;
import org.junit.Test;

public class CowTest {

    @Test
    public void eat_greens_sayYumYum() {
        Assert.assertEquals("greens! Yum yum :)", new Cow().eat("greens"));
    }

}